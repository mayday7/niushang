$(function () {

    var timer;
    $('.city a').hover(function () {
        $('.select').show();
    }, function () {
        timer = setTimeout(function () {
            $('.select').hide();
        }, 200)
    })

    $('.select').hover(function () {
        clearTimeout(timer);
        $(this).show();
    }, function () {
        $(this).hide();
    })


    /*
    var timer = null;
    $('.city a').mouseover(show);
    $('.city a').mouseout(hide)
    $('.select').mouseover(show);
    $('.select').mouseout(hide)

    function show() {
        clearTimeout(timer);
        $('.select').show();
    };
    function hide() {
        timer = setTimeout(function () {
            $('.select').hide();
        }, 300)
    }
*/

    $('.con2_left').mouseover(function () {
        $(this).children().show();
});
//旋转木马

    //设置耳朵隐藏显示
    // 2. 设置显示和隐藏
    $('.slider').mouseover(function () {
        $('.slider_ctl').show();
        //二维码
    })
    $('.slider').mouseout(function () {
        $('.slider_ctl').hide();
        $('.slider_main a span').hide()

    })
    var lis=$('.slider_main').children('li');
   lis.each (function () {
         $(this).children('span').slideUp().siblings().children('span').hide()

   })
    console.log(lis)
    console.log('-------------')



    /*$('.slider_main>li').mouseover(function () {
        $(this).children('span').show().siblings().children('span').hide();
    })*/

    var allLis = $('.slider_main').children();
    //节流
    var flag=true;
    //用json设置位置
    var json = [
        {   //  1
            num:1,
            transform: 'translateZ'+'('+'-300px'+') '+ 'rotateY('+'45deg'+')',
            width:256,
            top:0,
            left:-127,
            opacity:0.6,
            z:2
        },
        {  // 2
            num:2,
            transform: 'translateZ'+'('+'-200px'+') '+ 'rotateY('+'45deg'+')',
            width:256,
            top:0,
            left:50,
            opacity:0.6,
            z:3
        },
        {   // 3
            num:3,
            transform: 'translateZ'+'('+'0'+') '+ 'rotateY('+'0'+')',
            width:256,
            left: 303,
            top:0,
            opacity:1,
            z:4
        },
        {  // 4
            num:4,
            transform: 'translateZ'+'('+'-200px'+') '+ 'rotateY('+'-45deg'+')',
            width:256,
            top:0,
            left:540,
            opacity:0.6,
            z:3
        },
        {   //5
            num:5,
            transform: 'translateZ'+'('+'-300px'+') '+ 'rotateY('+'-45deg'+')',
            width:256,
            top:0,
            left:735,
            opacity:0.6,
            z:2
        }
    ];

    //赋值

    changePosition();
    for (var i =0; i < json.length; i++){
        console.log(json[i].num);

    }
    console.log("------")

    var timelater;

    //右边耳朵
    $('.slider_ctl_next').mousedown(function () {
        clearTimeout(timelater)
        timelater = setTimeout(function () {
            json.unshift(json.pop());
            flag = false;
            changePosition();
        },300)

    })
    //左边耳朵
    $('.slider_ctl_prev').mousedown(function () {
        clearTimeout(timelater)
        timelater = setTimeout(function () {
            json.push(json.shift());
            flag = false;
            changePosition();
        },300)

    })







    function buffer(obj, json, fn) {
        // 3.1 清除定时器
        clearInterval(obj.timer);
        // 3.2 设置定时器
        var begin = 0, target = 0, speed = 0;
        obj.timer = setInterval(function () {
            // 3.2.0 定义一个旗帜
            var flag = true; // 记录是否清除定时器
            for(var k in json){
                // 3.2.1 求出初始值
                if('opacity' == k){ // 透明度
                    begin = getCSSAttrValue(obj, k) == 0 ? 0 : parseInt(parseFloat(getCSSAttrValue(obj, k))*100) || 100;
                    target = parseInt(json[k]*100);
                    // console.log(begin, target);
                }else if('scrollTop' == k){ // 其他情况
                    begin = obj.scrollTop;
                    target = parseInt(json[k]);
                    /*console.log(begin, target);*/
                }else { // 其他情况
                    begin = parseInt(getCSSAttrValue(obj, k)) || 0;
                    target = parseInt(json[k]);
                }
                // 3.2.2 求出步长
                speed = (target - begin) / 20;
                // 取整(向上 向下)
                speed = (target > begin) ? Math.ceil(speed) : Math.floor(speed);
                // 3.2.3 动起来
                if('opacity' == k){
                    // 其他浏览器
                    obj.style.opacity =  (begin + speed) / 100;
                    // IE
                    obj.style.filter = 'alpha(opacity: '+ (begin + speed) +')';
                }else if('zIndex' == k){ // 层级结构
                    obj.style[k] = json[k];
                }else if('scrollTop' == k){ // 滚动
                    obj.scrollTop = begin + speed;
                }
                else if ("transform" == k){
                    obj.style[k] = json[k]
                }
                else {
                    obj.style[k] = begin + speed + 'px';
                }
                // 3.2.4 判断--> 只要有一个属性的动画没有到位就一定不能清除定时器
                if(target != begin){
                    flag = false;
                }
            }

            // 3.3 清除定时器
            if(flag){
                clearInterval(obj.timer);
                // 判断有没有回调函数
                if(fn){
                    // 只要定时器被清除了,就应该执行回调函数
                    fn();
                }
            }
        }, 20);
    }


    /*
     *  用于获取属性的属性值
     *  obj: 对象
     *  attr: 对象的属性
     */
    function getCSSAttrValue(obj, attr) {
        if(obj.currentStyle){
            return obj.currentStyle[attr];
        }else {
            return window.getComputedStyle(obj, null)[attr];
        }
    }




//位置改变
    function changePosition() {
        for(var i=0; i<json.length; i++){
            var item=json[i];
            buffer(allLis[i],
            {
                width:item.width,
                    top:item.top,
                    left:item.left,
                    opacity:item.opacity,
                    zIndex:item.z,
                    transform:item.transform
            }, function () {
                flag = true;
            })

        }
    }










});