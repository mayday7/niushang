/**
 * Created by ying on 2017/3/2.
 */

window.onload = function () {
        // 1. 获取需要的标签
        var slider = $('slider');
        var slider_main = $('slider_main');
        var slider_ctl = $('slider_ctl');
        var allLis = slider_main.children;
        // 节流
        var flag = true;

        // 2. 设置显示和隐藏
        slider.onmouseover = function () {
            buffer(slider_ctl, {opacity:1});
        }
        slider.onmouseout = function () {
            buffer(slider_ctl, {opacity:0});
        }

        // 3. 定位
        // 3.1 位置信息
        var json = [
            {   //  1
                num:1,
                transform: 'translateZ'+'('+'-300px'+') '+ 'rotateY('+'45deg'+')',
                width:256,
                top:0,
                left:-127,
                opacity:0.6,
                z:2
            },
            {  // 2
                num:2,
                transform: 'translateZ'+'('+'-200px'+') '+ 'rotateY('+'45deg'+')',
                width:256,
                top:0,
                left:50,
                opacity:0.6,
                z:3
            },
            {   // 3
                num:3,
                transform: 'translateZ'+'('+'0'+') '+ 'rotateY('+'0'+')',
                width:256,
                left: 303,
                top:0,
                opacity:1,
                z:4
            },
            {  // 4
                num:4,
                transform: 'translateZ'+'('+'-200px'+') '+ 'rotateY('+'-45deg'+')',
                width:256,
                top:0,
                left:540,
                opacity:0.6,
                z:3
            },
            {   //5
                num:5,
                transform: 'translateZ'+'('+'-300px'+') '+ 'rotateY('+'-45deg'+')',
                width:256,
                top:0,
                left:735,
                opacity:0.6,
                z:2
            }
        ];

        // 3.2 赋值
        changePosition();
        for (var i =0; i < json.length; i++){
            console.log(json[i].num);

        }
        console.log("------")
        // 4.监听点击
//        for(var i=0; i<slider_ctl.children.length; i++){
//            var item = slider_ctl.children[i];
//            item.onmousedown = function () {
//                if(flag){
//                    if(this.className == 'slider_ctl_prev'){ //点击了左边
//                        /*移出位置数组中的第一个放到最后一个*/
//                        json.push(json.shift());
//                    }else {
//                        /*移出位置数组中的最后一个放到第一个*/
//                        json.unshift(json.pop());
//                    }
//                    flag = false;
//                    changePosition();
//                }
//                for (var i =0; i < json.length; i++){
//                    console.log(json[i].num);
//                }
//                console.log("------")
//            }
//        }
        var conRight = document.querySelector('.slider_ctl_next');

        var timelater;
        conRight.onmousedown = function () {
            clearTimeout(timelater)
            timelater = setTimeout(function () {
                json.unshift(json.pop());
                flag = false;
                changePosition();
            },300)

        }




        // 5.位置改变
        function changePosition() {
            for(var i=0; i<json.length; i++){
                var item = json[i];
                buffer( allLis[i], {
                    width:item.width,
                    top:item.top,
                    left:item.left,
                    opacity:item.opacity,
                    zIndex:item.z,
                    transform:item.transform
                }, function () {
                    flag = true;
                })
            }
        }
    }
